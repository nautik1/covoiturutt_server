<?php

/**
 * Description of OtherTablesSeeder
 *
 * @author Nico
 */
class OtherTablesSeeder extends Seeder {
    
    public function run()
    {
        DB::table('place')->delete();
        DB::table('place')->insert(array(
            'id' => 1,
            'displayed_name' => 'UTT'
        ));
        DB::table('place')->insert(array(
            'id' => 2,
            'displayed_name' => 'Centre-ville'
        ));
        DB::table('place')->insert(array(
            'id' => 3,
            'displayed_name' => 'Gare'
        ));
        DB::table('place')->insert(array(
            'id' => 4,
            'displayed_name' => 'Hopital'
        ));
        DB::table('place')->insert(array(
            'id' => 5,
            'displayed_name' => 'Leclerc'
        ));
        
        DB::table('trip')->insert(array(
            'id' => 1,
            'driver_id' => 3,
            'departure_place_id' => 1,
            'destination_id' => 2,
            'departure_time' => '14-01-2014 15:30:00',
            'total_seats' => 3,
            'payment_asked' => '1 café'
        ));
        
        DB::table('trip')->insert(array(
            'id' => 2,
            'driver_id' => 2,
            'departure_place_id' => 1,
            'destination_id' => 2,
            'departure_time' => '14-01-2014 16:10:00',
            'total_seats' => 3,
            'payment_asked' => 'gratuit'
        ));
        
        DB::table('trip')->insert(array(
            'id' => 4,
            'driver_id' => 3,
            'departure_place_id' => 1,
            'destination_id' => 2,
            'departure_time' => '14-01-2014 16:30:00',
            'total_seats' => 3,
            'payment_asked' => '1 pizza'
        ));
        
        DB::table('trip')->insert(array(
            'id' => 3,
            'driver_id' => 2,
            'departure_place_id' => 2,
            'destination_id' => 1,
            'departure_time' => '15-01-2014 08:30:00',
            'total_seats' => 3,
            'payment_asked' => 'gratuit'
        ));        
    }
}

?>
