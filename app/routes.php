<?php

// Routes for auth-token plugin
Route::get('auth', 'Tappleby\AuthToken\AuthTokenController@index');
Route::post('auth', 'Tappleby\AuthToken\AuthTokenController@store');
Route::delete('auth', 'Tappleby\AuthToken\AuthTokenController@destroy');

// Route group for API versioning, protected by token.
Route::group(array('before' => 'auth.token'), function()
{
    // Routes for all actions related to user.
    Route::get('user/get-participation-trips', 'UserController@getParticipationTrips');
    Route::get('user/get-offered-trips', 'UserController@getOfferedTrips');
    Route::get('user/store-registration-id', 'UserController@storeRegistrationId');
    
    Route::get('trip/search', 'TripController@searchTrips');
    Route::get('trip/subscribe', 'TripController@subscribe');
    Route::get('trip/unsubscribe', 'TripController@unsubscribe');
    Route::get('trip/change-participant-status', 'TripController@changeParticipantStatus');
    
    Route::get('place/all', 'PlaceController@getAll');
});