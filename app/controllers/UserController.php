<?php

use Illuminate\Database\Eloquent\Collection;

class UserController extends \BaseController {
    
    public function getParticipationTrips() {
        
        $user = Auth::user();
        
        // Get trips where user is participant
        $tripParticipations = $user->tripParticipations;
        $tripParticipations->load('driver');
        $tripParticipations->load('destination');
        $tripParticipations->load('departurePlace');

        return $tripParticipations->toArray();
    }
    
    public function getOfferedTrips() {
        
        $user = Auth::user();
        
        // Get trips where user is driver
        $tripsOfferedAsDriver = $user->tripOfferedAsDriver;
        $tripsOfferedAsDriver->load('driver');
        $tripsOfferedAsDriver->load('destination');
        $tripsOfferedAsDriver->load('departurePlace');
        $tripsOfferedAsDriver->load('participants');

        return $tripsOfferedAsDriver->toArray();
    }

    public function storeRegistrationId() {
        $user = Auth::user();
        $androidRegistrationId = \Input::get('registration_id');
        
        $user->android_registration_id = $androidRegistrationId;
        $user->save();
        
        return array('status' => 0);
    }
    
    public function getService() {
        return "UserController";
    }
    
}