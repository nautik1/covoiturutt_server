<?php

abstract class BaseController extends Controller {

   protected $result = null;
 
   abstract public function getService();

    /**
    *  formate la reponse par defaut
    **/
    public function __construct()
    {
        $this->result= new stdClass();
        $this->result->error= 0;
        $this->result->message= '';
        $this->result->service= $this->getService();
        $this->result->data = null;
    }
    
    protected function render()
    {
        return Response::json($this->result);
    }
    
    protected function run($function)
    {
        try {
            $this->result->data= call_user_func($function);
        } catch (Exception $e) {
            $this->result->err = ($e->getCode()>0) ? $e->getCode() : -1;
            $this->result->message = $e->getMessage();
        }
        return $this->render();
    }
}