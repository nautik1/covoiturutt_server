<?php

require app_path().'/config/googleApiAuth.php';
use \Symfony\Component\Routing\Exception\InvalidParameterException;

define("MESSAGE_TYPE_DRIVER_CHANGED_PARTICIPANT_STATUS", 0);
define("MESSAGE_TYPE_USER_SUBSCRIBED_TO_TRIP", 1);
/**
 * Description of TripController
 *
 * @author Nico
 */
class TripController extends \BaseController {
    
    public function getService() {
        return "TripController";
    }
    
    public function searchTrips() {
        $departureId = \Input::get('departure_place_id');
        $destinationId = \Input::get('destination_id');
        $date = urldecode(\Input::get('date'));
        
        if (is_null($date) || is_null($departureId) || is_null($destinationId)) {
            throw new InvalidParameterException;
        }
        
        // Search for trips matching the parameters
        $matchingTrips = Trip::with('driver', 'destination', 'departurePlace')
                ->where('departure_time', '>=', $date)
                ->where('departure_place_id', '=', $departureId)
                ->where('destination_id', '=', $destinationId)
                ->orderBy('departure_time')
                ->get();
        
        return $matchingTrips->toArray();
    }
    
    public function subscribe() {
        $tripId = \Input::get('trip_id');
        $user = Auth::user();
        
        $tripFound = Trip::find($tripId);
        if (is_null($tripFound)) {
            return array('status' => 2);
        }
        
        $tripParticipation = TripParticipation::where('trip_id', '=', $tripId)
                ->where('user_id', '=', $user->id)
                ->first();
        if (! is_null($tripParticipation)) {
            return array('status' => 1);
        }
        
        $tripParticipation = new TripParticipation;
        $tripParticipation->trip_id = $tripId;
        $tripParticipation->user_id = $user->id;
        $tripParticipation->status = 0;
        
        $tripParticipation->save();
        
        $this->sendNotification(MESSAGE_TYPE_USER_SUBSCRIBED_TO_TRIP, $user, $tripFound->driver);
        return array('status' => 0);
    }
    
    public function unsubscribe() {
        $tripId = \Input::get('trip_id');
        $user = Auth::user();
        
        $tripFound = Trip::find($tripId);
        if (is_null($tripFound)) {
            return array('status' => 2);
        }
        
        $tripParticipation = TripParticipation::where('trip_id', '=', $tripId)
                ->where('user_id', '=', $user->id)
                ->first();
        if (is_null($tripParticipation)) {
            return array('status' => 1);
        }
        
        $tripParticipation->delete();
        return array('status' => 0);
    }
    
    private function sendNotification($notificationType, $initiator, $receiver) {
        
        $registrationId = $receiver->android_registration_id;
        if ( !is_null($registrationId) && (strcmp($registrationId, "") != 0) ) {
            $dataToTransmit = json_encode(array(
                'registration_ids' => array($receiver->android_registration_id),
                'data' => array(
                    'notificationType' => $notificationType,
                    'initiator' => $initiator->displayed_name
                    )
                ));

            $ch = curl_init('https://android.googleapis.com/gcm/send');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataToTransmit);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: key=' . GOOGLE_API_KEY,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($dataToTransmit))
            );

            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);
        }
    }
    
    public function changeParticipantStatus() {
        $loggedUser = Auth::user();
        $tripId = \Input::get('trip_id');
        $participantId = \Input::get('participant_id');
        $newStatus = \Input::get('new_status');
        
        // Check if trip exists and if user logged is allowed to accept or refuse a participant (is driver)
        $tripFound = Trip::find($tripId);
        if (is_null($tripFound) || $tripFound->driver->id != $loggedUser->id) {
            return array('status' => 4);
        }
        
        // Check if participant exists
        $participantFound = User::find($participantId);
        if (is_null($participantFound)) {
            return array('status' => 3);
        }
        
        // Check if the participant is participating to this trip.
        $tripParticipation = TripParticipation::where('trip_id', '=', $tripId)
                ->where('user_id', '=', $participantId)
                ->first();
        if (is_null($tripParticipation)) {
            return array('status' => 2);
        }
        
        // Check if the new status is valid.
        $possibleValues = array(1, 2);
        if (!in_array($newStatus, $possibleValues)) {
            return array('status' => 1);
        }
        
        // Save new status.
        $tripParticipation->status = $newStatus;
        $tripParticipation->save();
        
        $this->sendNotification(MESSAGE_TYPE_DRIVER_CHANGED_PARTICIPANT_STATUS, $loggedUser, $participantFound);
        
        return array('status' => 0);
    }
}

?>
