<?php
/**
 * Description of TripParticipant
 *
 * @author Nico
 */
class TripParticipation extends Eloquent {
    
    protected $table = 'trip_participation';
    
    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function trip()
    {
        return $this->belongsTo('Trip');
    }
}

?>
