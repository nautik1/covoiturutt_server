Covoiturutt - server side
=========================

API services available:
-----------------------

###Authentication:
Url:  
    /auth

####POST request:
Ask for authentication.
* Required parameters: username, password
* Return: a token to use for future requests.

####GET request:
Returns information about the user corresponding to the token.
* Required parameters: auth_token
* Return: User.

-----------------------------------------------------------------------------------


** IMPORTANT: all the services below are token-protected. For every call is required the parameter: auth_token.
All the returns are JSON encoded.**

###User-related services:

####Get trips of the user
Url:  
    /user/get-my-trips

Allows to retrieve the trips where the user is subscribed as participant and those offered as driver.
* Required parameters:
* Return: array with 2 lists. First is "tripParticipations" and second is "tripsOfferedAsDriver". Both containing Trip instances.

###Trip-related services:

####Search for trips
Url:  
    /trip/search

Look for trips considering various parameters.
* Required parameters: date (url-encoded format "yyyy-MM-dd hh:mm:ss"), departure_place_id and destination_id
* Return: List of Trip instances ordered by departure date.

###Places-related services:
####Get all available places
Url:  
    /place/all

Get all available place for trips.
* Required parameters: 
* Return: List of Place instances.
